import React from 'react';


function Button(props) { //function Button({ label, color })
  // { label } = props;
  return (
  <button type={props.type}>{props.children}</button>
  );
}

Button.defaultProps = {
  label: 'Enter label',
  type: 'submit'
}

export default Button;